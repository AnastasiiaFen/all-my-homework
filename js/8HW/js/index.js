"use strict"
// 1.
let changeParagr = document.querySelectorAll('p');
changeParagr.forEach(element => {
    element.style.color = "#ff0000"
});
// 2.
let idList = document.querySelector("#optionsList");
console.log(idList);
let parentIdList = idList.parentNode;
console.log(parentIdList);
let childIdList = idList.childNodes;
childIdList.forEach(element =>
    console.log(element.nodeName));

childIdList.forEach(element =>
    console.log(element.nodeType));


// 3.

let someParagr = document.querySelector('#testParagraph ');
someParagr.textContent = 'This is a paragraph';

// 5.

let someElements = document.querySelector('.main-header');

let childSomeElements = [...someElements.children];
console.log(childSomeElements);
childSomeElements.forEach(element => {
    element.classList.add('nav-item')
});

