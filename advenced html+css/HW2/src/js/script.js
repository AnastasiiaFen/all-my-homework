const btnClick = event => {
    const navList = document.querySelector(".header__nav")
    navList.classList.toggle("hide")
}

document.querySelector(".btn").addEventListener("click", btnClick)

const imageClick = event => {
    const navImageCross = document.querySelector(".header__menu-cross")
    const navImage = document.querySelector(".header__menu")
    navImageCross.classList.toggle("invisible")
    navImage.classList.toggle("invisible")
}
document.querySelector(".btn").addEventListener("click", imageClick)

