const tabs = document.querySelectorAll('.tabs-title');
const content = document.querySelectorAll('.tabs-content-item');

tabs.forEach(tab => tab.addEventListener('click', tabClick));

function tabClick(event) {
    const tabActive = event.target.dataset.current;

    tabs.forEach((tab, i) => {
        tab.classList.remove('active');
        content[i].classList.remove('active');
    })

    tabs[tabActive - 1].classList.add('active');
    content[tabActive - 1].classList.add('active');
}

const filterBox = document.querySelectorAll('.image_changer_item');

document.querySelector('nav').addEventListener('click', (event) => {

    if (event.target.tagName !== 'LI') return false;
    let filterClass = event.target.dataset['f'];
    // event.target.classList.add('border')

    filterBox.forEach(elem => {
        elem.classList.remove('hide');
        if (!elem.classList.contains(filterClass) && filterClass !== 'all') {
            elem.classList.add('hide');
        }
    });

});

const btnImageLoader = document.querySelector('.btn_three');
btnImageLoader.addEventListener('click', (event) => {
    if (event.target) {
        document.querySelector('.two_image_box').classList.remove('two_image_box')
        btnImageLoader.classList.add('hidden')
        // loaderSpiner.classList.remove('hidden')
    }
})
// const loaderSpiner = document.querySelector('.lds-ring');
// const removeHiddenClass = function(){loaderSpiner.classList.remove('hidden')}
// let timerId = setTimeout(removeHiddenClass, 6000)

const workersFoto = [
    '../image/Новая папка 3/Layer 6.png',
    '../image/Новая папка 3/Layer 6.png)',
    '../image/Новая папка 3/Layer 8.png',
    '../image/Новая папка 3/Layer 344.png',
];
const btnPrev = document.querySelector('.btn-previous');
const btnNext = document.querySelector('.btn-next');
let currentImg = 0;
const imgContainer = document.querySelector('.image_container');
btnPrev.addEventListener('click', () => {
    if (currentImg > 0) {
        currentImg -= 1
        imgContainer.style.backgroundImage = `url("${workersFoto[currentImg]}")`
    }
})
btnNext.addEventListener('click', () => {
    if (currentImg < workersFoto.length - 1) {
        currentImg += 1
        imgContainer.style.backgroundImage = `url("${workersFoto[currentImg]}")`
    }
})
const workersFotoClicker = document.querySelectorAll('.mini_foto')
workersFotoClicker.forEach(foto => foto.addEventListener('click', fotoClick));
function fotoClick(event) {
    const tabActive = event.target.dataset.foto;
    switch (tabActive) {
        case '1':
            imgContainer.style.backgroundImage = `url("${workersFoto[0]}")`;
            break;
        case '2':
            imgContainer.style.backgroundImage = `url("${workersFoto[1]}")`;
            break;
        case '3':
            imgContainer.style.backgroundImage = `url("${workersFoto[2]}")`;
            break;
        case '4':
            imgContainer.style.backgroundImage = `url("${workersFoto[3]}")`;
            break;
    }
}
