"use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get() {

    }
    set() {

    }
}
class  Programmer extends Employee {
    constructor(lang, ...args) {
        super(...args);
        this.lang = lang;
    }
    getSalary() {
        return this.salary*3;
    }
}

const FirstProgrammer = new Programmer('JS','Jonh', 25, 5000)
console.log(FirstProgrammer);
console.log(FirstProgrammer.getSalary());
const SecondProgrammer = new Programmer('CSS','Bob', 30, 2000)
console.log(SecondProgrammer);
const ThirdProgrammer = new Programmer('JS','Tom', 40, 7000)
console.log(ThirdProgrammer);
