import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import browserSync from 'browser-sync';

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const buildStyles = () => gulp.src('./scr/styles/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dest/css'));

export const dev = gulp.series(buildStyles, () => {
    BS.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('./src/**/*', gulp.series(buildStyles, (done) => {
        BS.reload();
        done()
    }))
})