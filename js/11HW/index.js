const input = document.querySelector(".password");
const inputRepeat = document.querySelector(".password-repeat");
const form = document.querySelector(".password-form")
const btn = document.querySelector(".submit")

const eyesFirst = document.querySelectorAll(".first");
const eyesRepeat = document.querySelectorAll(".repeat");

eyesFirst.forEach(tab => tab.addEventListener('click', tabClick));
eyesRepeat.forEach(tab => tab.addEventListener('click', tabClickRepeat));

function tabClick(e) {
        if (input.getAttribute('type') == 'password') {
            eyesFirst[1].classList.add('active');
            input.setAttribute('type', 'text');
        } else {
            eyesFirst[1].classList.remove('active');
            input.setAttribute('type', 'password');
        }
        return false;
    }

function tabClickRepeat(e) {
    if (inputRepeat.getAttribute('type') == 'password') {
        eyesRepeat[1].classList.add('active');
        inputRepeat.setAttribute('type', 'text');
    } else {
        eyesRepeat[1].classList.remove('active');
        inputRepeat.setAttribute('type', 'password');
    }
    return false;
}

const checkPassword = (e) => {
    e.preventDefault()
    if(input.value !== inputRepeat.value){
        document.querySelector('.error').innerHTML = 'Потрібно ввести однакові значення'
    } else
    alert('You are welcome');
}
form.addEventListener("submit", checkPassword)