

const tabs = document.querySelectorAll('.tabs-title');
const content = document.querySelectorAll('.tabs-content-item');

tabs.forEach(tab => tab.addEventListener('click', tabClick));

function tabClick(event) {
    const tabActive = event.target.dataset.current;

    tabs.forEach((tab, i) => {
        tab.classList.remove('active');
        content[i].classList.remove('active');
    })

    tabs[tabActive - 1].classList.add('active');
    content[tabActive - 1].classList.add('active');
}



