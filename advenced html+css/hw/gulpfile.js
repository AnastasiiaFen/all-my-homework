import gulp from 'gulp';
import browserSync from 'browser-sync';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';


const BS = browserSync.create();
const sass = gulpSass(dartSass);


const buildStyles = () => gulp.src('./src/styles/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css'));

export const dev = gulp.series(buildStyles, () => {
    BS.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('./src/**/*', gulp.series(buildStyles, (done) => {
        BS.reload();
        done();
    }))
})


